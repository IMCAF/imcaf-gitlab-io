---
title: "MHM - Motor Health Monitoring"
cascade:
  featured_image: '/UC_V_FundoClaro-negro.png'

description: ""
---

This was a Project made for the Project II course in the University of Coimbra,
it consisted of making a smart current sensor which detected problems in electrical motors
so that they could be solved before they led to catastrofic failure and factory downtime.

