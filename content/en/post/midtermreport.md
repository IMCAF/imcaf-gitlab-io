---
date: 2022-04-09T10:58:08-04:00
description: "First Progress report for the IMCAF project "
featured_image: '/UC_V_FundoClaro-negro.png'
tags: ["scene"]
title: "Mid Term Report"
---

# Introduction

With the Electrical revolution, the world gained a much more powerful and efficient way to create motion, the AC electrical motor even to this day all throughout the diverse industries one of the constants is the reliance on these powerful electrical motors, be it either for powering heavy machinery, pumping liquids or gasses, Conveyors belts, lifts, trains, etc. But as reliable as they are, they are still susceptible to breaking down due to failures of its bearings, windings, overheating, etc. Such failures can easily lead to significant operational downtime and thus loss of production efficiency and profit, regular maintenance can reduce the risk of failure but it doesn’t eliminate it, luckily most of these failures aren’t caused by instantaneous events, instead they are caused by deterioration overtime of various components. Thus by closely monitoring the motor it's possible to detect the deterioration before it leads to catastrophic failure.
 	The purpose of the MHM is to do the sensing and signal processing needed and later transmitting to any device with internet access the current  state of the electric motors in real time, so that an user who can, based on this, act in order to prevent damages from happening, or that in case they do happen, reduce the severity of them. Thanks to the reading being done through non-invasive sensors, it avoids security and reliability concerns, maintains safety and simplifies the installation of the monitoring system. This report describes the main tasks, what work has been done, the changes in relation to the original plan and also includes the work plan for the coming months.

# Summary 

Quick theory of operation, three current signals read by the current transformers are amplified and then sent simultaneously to a three channel ADC to be digitized.
In the Microprocessor, the signals are read from the ADC, and are then processed using ffts and integrals, to obtain the necessary variables to determine the state of the motor. The result is then sent to the cloud, accessible by the user anywhere with internet access. For the security minded we also support streaming to a local network computer, or direct readings on the LCD, keeping the data offline.
There are three main aspects to this project, the hardware, the signal processing in the µController and the end user interface which could be the LCD on the board, a site online, or even a program on the user’s computer.  According to the schedule that we had planned and mentioned in the previous report, we have successfully completed the hardware design (tasks 1 to 5), we are currently  working on the assembly, test and calibration of the board (task 6) (missing the LCD screen), we also began testing the wifi connection on a breadboard prototype (task 7) with good initial results.
 We are currently on schedule to deliver the rest of the tasks.
 
 # Project status

The elaboration of the project involved designing a schematic and PCB board, this took us about 16 hours to be completed.
 At that time we elaborated the project proposal report and we received our supervisors feedback and orientation. 
Then we had to do our search for components that would satisfy our signal conditioning specifications (such as resistors, amplifiers, voltage regulators, transistors, diodes, etc). After selecting the hardware, we bought it and waited for it to arrive, and this wait delayed our progress for a few days, so the second milestone (Assemble, Test and Calibrate board, April 5th)  was delivered 2 days late, however the first one was on time(Design the Schematic and run spice simulation, March 15th). 
Finally we managed to assemble the board and we are currently in the testing phase, while simultaneously figuring out the connection between the board and the internet (wi-fi) using a prototype on a breadboard.
Tasks 8 to 11 should be started as soon as we are done with the arduino to wi-fi connection.

{{< mermaid align="center" theme="forest" >}}
gantt
    title Work Schedule
    dateFormat  DD-MM-YYYY
    section Design
      R&D Front End/ADC  :done, a1, 05-03-2022, 7d
      Define Project's specs                 :done, after a1  , 7d
      Select Hardware                        :done, after a1  , 14d
      Design Front End                       :done, after a1  , 14d
      Schematic, PCB, SIM                    :done, after a1  , 14d
    section Build Prototype
      Assembly                               :done, a2, 19-03-2022, 7d
      Test                                   :active, a3, after a2, 7d
      Calibration                            :active, after a3  ,  14d
    section Software
      Comms to ADC and WIFI                  :active, after a3  ,  21d
      Signal Analysis Programming            :09-04-2022  , 28d
      Program Cloud Data Stream              :16-04-2022  , 21d
    section Final
      Final Test, Misc                       :30-04-2022  , 14d
      Technical Manual                       :30-04-2022  , 14d
{{< /mermaid >}}

## 1 - Research Signal conditioning 

At this stage we need to be able to shape the signal read by the sensors in order to obtain one that we can use. We had to attenuate, amplify, eliminate noise and use a signal clamping, which we had to learn that it is an electronic circuit that suppresses overvoltages above its breakdown voltage by using TVS (transient voltage) diodes, that act really quickly (some as fast as 50ps).
We also had to familiarize ourselves with how fully differential amplifiers work (they basically have differential inputs and differential outputs) as well as what anti-aliasing filters are and how to implement them (it is a filter used to restrict the bandwidth of a signal preventing the adc from “seeing” reflections of higher frequency noise at the frequency of interest).

## 2 - Basic block diagram

{{< mermaid align="center" theme="neutral" >}}
graph TD;
    A(Sensor)-->B(Signal conditioning);
    B-->C(ADC);
    C-->|SPI0|D[Raspberry];
    E(Rotary Encoder)-->D;
	D-->|SPI1|F(Radio);
	D-->|Uart|G(WiFi);
	D-->|I2C|H(LCD);
{{< /mermaid >}}

## 3 - Select the hardware for each block
### 3.1 - ADC

Analog to Digital converter will set the base specifications of how accurate and how much throughput this monitoring system will have, the most important specifications are effective resolution (bits), throughput in Samples per second per channel, noise performance, number of channels, Full Scale Range (Recommended maximum input voltage), the chosen µController could be the limiting factor at high throughput so ADC performance over the benchmarks results wont be taken advantage of.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Specification          | ADE7454 | MCP3564R | MCP3903 | ADS131M03 |
|-----------------------|:-------:|:--------:|:-------:|:---------:|
| Nº of channels        |    6    |    4     |    6    |     3     |
| Resolution            |  24-Bit |  24-Bit  |  24-Bit |   24-Bit  |
| Effective Resolution  |         |  15 Bits | <p style="color :green; ">20 Bits</p>  | 16.7 Bits |
| Master Clock          |         |  20Mhz   |         | 4.096Mhz  |
| Samples/second*       | 26KS/s  | <p style="color :green; ">51.2KS/s</p> | 15KS/s  |   <p style="color:orange">32KS/s</p>  |
| Over Sampling Ratio   |         |    32    |   64    |    <p style="color :green; ">128</p>     |
| FS range              |  ±0.5v  |  <p style="color :green; ">±3.3v</p>   |  ±0.5v  |   <p style="color:orange">±1.2v</p>    |
| Supply Voltage        |   5v    |   3.3v   | A 5v/Dig 3.3v |  3.6v  |
| Cost                  | 11.76€  |  <p style="color :green; ">3.86€</p>    |  4.53€  |   4.22€   |
{{</table>}}
* Samples per second per channel, as some ADCS divide their Samples/s by the number of active channels


The ADS131M03IPWR using a TSSOP-20 package was chosen for its simultaneous sampling, simple to use spi communication, interrupt support, and simple to solder footprint.
We were later forced to change the ADC part as it was out of stock by the time we were placing the order, luckily a different footprint of the same ADS131M03 was still available the ADS131M03IRUKR which only required small changes to the PCB board, this one used a QFN-20 package, which was much smaller and thus harder to solder, to alleviate this issue the team made use of a stainless steel stencil and solder paste. 

### 3.2 - Current sensor

There are 3 main types of sensor: shunt resistors, hall current and current transformers.
Shunt resistors are the cheapest option but they offer no electrical isolation which makes the ADC block much more complex for a 3 phase design.
Hall current sensors are the second cheapest option with good performance and isolation, but they require some careful thought due to thermal management, and high voltage on the pcb, so they will be avoided for this project.
Current transformers, they transform the measured current into a 100/500/2000/etc times smaller isolated current, a burden resistor is required (internal or external) to provide instead a voltage proportional to the current being measured there are 3 main types: Split core, air core and solid core. Care must be taken so that the burden resistor is present at all times or that the conductor being measured doesn’t have current flowing through it, as without it a high voltage may be outputted by the current transformer.
Air-core like Rogowski coils, these sensors usually need an integrator, but are more flexible in how they are attached to the conductor being measured, and have good frequency response.
Solid-core is usually used in more permanent use cases, as they require the conductor to be unplugged and inserted into the current transformer.
Split core current transformers allow the core to open facilitating the insertion of the conductor without the need to unplug it.
Due to the inherent safety of current transformers, the chosen sensor will be of this type, from the various CTs split core allow for greater flexibility, thus the team chose the SCT-013-30A ⁽²⁾.

### 3.3 - Microprocessor

Minimum requirements:

- Spi support (for the ADC)
- Uart support (for the WIFI)
- Enough processing power and memory to support 2k+ Samples/s, per current sensor
- Arduino IDE support

 {{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}

|Specification          | Arduino Mega<br> 2560  | Arduino Mega<br> Pro mini 2560 | Arduino Nano | Arduino Nano<br> Every | Raspberry<br> Pico | ESP32 |
|----------------------|:----------:|:---------:|:---------:|:---------:|:-------:|:----------:|
| Architecture          | AVR 8 bit | AVR 8 bit | AVR 8 bit | AVR 8 bit | Arm M0+ | Xtensa LX6 |
| Width                 |   8-Bit   |   8-Bit   |   8-Bit   |   8-Bit   |  32-Bit |  32-Bit    |
| Cores                 |     1     |     1     |     1     |     1     |    2    |      2     |
| Core Frequency        |   16Mhz   |   16Mhz   |   16Mhz   |   20Mhz   | 125Mhz  | 80-240Mhz  |
| RAM                   |    8KB    |    8KB    |    2KB    |    6KB    | 264KB   |   512KB    |
| Serial Interfaces     | 1xSPI,<br>1xI2C<br>4xUART | 1xSPI,<br>1xI2C<br>4xUART | 1xSPI,<br>1xI2C<br>1xUART | 1xSPI,<br>1xI2C<br>4xUART | 2xSPI,<br>2xI2C<br>2xUART | 4xSPI,<br>2xI2C<br>3xUART |
| Supply Voltage        |    5v     |    5v     |    5v     |    5v     |  3.3v   |    3.3v    |
| ADC                   |   10-bit  |   10-bit  |   10-bit  |   10-bit  |  12-bit |  ~12-bit   |
| ADC                   |  4.8KS/s  |  4.8KS/s  |  4.8KS/s  |   ~6KS/s  |  73KS/s | (untested) |
| Cost                  |    35€    |    15€    |    14€    |    10€    |   5€    |    14€     |
{{</table>}}


A simple benchmark was made to have a rough idea of the computing performance of the available µcontroller’s , it comprises an approximation of the fft algorithm with 256 samples, and the result is how many samples could be calculated during a second of processor time.
Thanks to its really low cost and leading performance the Raspberry Pico was chosen.

## 4 - Design the signal conditioning network

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig4.png)


After the sensor and adc were chosen, the signal conditioning network was designed to bridge the two, and protect the board from signals outside of its safe range, the chosen sensor has a 82Ω internal burden resistor, still due to our limited range in the ADC (-1.2v to 1.2v), a second resistor was placed in parallel of 400Ω as to limit the input signal to 1.2v peak to peak, and help center the signal at 0V dc offset, to keep noise down a RC low pass filter was placed after the external burden resistor and before the amplification circuit, with a 150kHz -3db point, because the adc has differential inputs and so is the sensor, the team chose to have a fully differential network, using a fully differential amplifier the MCP6D11T with 90 MHZ bandwidth, rail to rail capability and 5.5v peak voltage range, which was supplied by a 3.3v Vdd and a -1.5v Vss guaranteeing that the ADC is protected at all times as it's within its safe range, the gain of the FDA was set to ~2 so as to reach 2.4V peak to peak when the sensor is sensing 30A rms, the FDA also has a Vocm input which sets the differential common mode voltage, in this case it was set to 0v. 
At the inputs to the adc a anti-aliasing filter was used according to the adc’s datasheet. Finally a TVS diode (UCLAMP2511T.TCT) between the sensor’s leads clamps the signal if needed to protect the amplifier and adc.
	The strange values of the resistors is due to what resistors are actually sold in large quantities in the market, changing the gain slightly doesn’t affect the performance of the board as it can be handled during calibration and it allows the reduction of the price of the resistors by nearly ten fold in one case, for the first round of boards the team used 0.5% accurate resistors across the board and C0G type capacitors for the filtering networks for its stable dielectric that doesn’t change capacitance with applied voltage, in the case of bypass capacitors we used X5R for its larger capacitance value in the same package. 
An interesting find while making this schematic was that initially R1 and RF had the same value at 1k as to keep the needed gain for the amplifier but later while simulating the circuit it was noticed that this meant that at higher frequencies the amplifier’s gain would increase from 2 to a limit of 4 slightly reducing the effectiveness of the RC filter, so most of the resistance value is now placed in R1 keeping the gain increase at high frequencies much smaller.

## 5 - Design the Schematic and run spice simulation

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig3.png)

##### fig. 3 - Power Circuitry Schematic

### 5.1 - Power Circuitry
With the rest of the circuit designed the team had enough data to design the power circuitry, most of the circuit runs of 3.3v, with the amplifiers also needing a -1.5v rail, because usb is 5v both, a 3.3v and a -1.5v regulators are needed, through max power consumption values taken from the various component’s datasheets, the team found the minimum current that each regulator needed to supply.

#### 5.1.1 - The 3.3V supply
The powering of our circuit is made through a USB-mini B connector as a 5V source, or a 2.54mm pitch header. Then we have to use voltage regulators, one for 3.3V powered by USB, and one for -1.5V powered by the 3.3V. This guarantees stability in the circuit and we can supply it with the range of voltages that the regulator supports (fig. 4).
	The 3.3V regulator powers the whole board, we analyzed the maximum current consumed by the amplifiers, the LCD, the ADC, µcontroller and wifi board and we reached a figure of slightly less than 1A, most regulators were rated for just 1A which was too close to maximum to be reliable, the next ones were 1.5A but were around the same price as the 3A AZ1084CD-3.3TRG1 so we chose it for peace of mind.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Recommended Operation Conditions                                               |
|-----------------------|:----------------:|:--------:|:--------:|:------------:|
|         Symbol        |     Parameter    |    Min   |    Max   |      Unit    |
|          Vin          |   Input Voltage  |    ---   |    12    |       V      |
|          Tj           |Operating Junction|     0    |    125   |      ºC      |
{{</table>}}

####### fig. 4 - max voltage that the 3.3V regulator can hold

#### 5.1.2 - The -1.5V supply

Our ADC works with differential inputs from -1.2V to 1.2V and from -1.6V its behavior is no longer as desired ⁽⁴⁾ we thought -1.5V was a good value to guarantee a good error margin. the ADCs use very little power so we chose the MAX1735EUK50+T regulator as it can provide 200mA(5) of current. For the charge pump it simply had to deliver enough current to feed the regulator, so we chose the SP6661EN-L_TR.

###### 5.1.3 - Protection against reverse polarity

Here we used a P-channel Mosfet (SI4435FDY-T1-GE3) that acts as a high speed switch that only turns on with the right polarity, thus protecting the circuit from reverse polarity. According to the mosfet’s datasheet the circuit should be protected down to -30V ⁽⁵⁾
	
### 5.2 - User interface
The user interface was done via a Rotary encoder, with a integrated push button allowing us to implement a simple and intuitive interface, a Rc filter on each button guarantees that quick bounces while the button is settling won't affect the signal read by the µcontroller, which occupied 3 digital inputs of the raspberry pico. 
There is also an I2C port to allow the use of various lcd screens.

### 5.3 - Final Schematic

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig5.png)

####### fig. 5 - Main Schematic

Starting from the Analog side, using standard audio jack connectors, the sensor signals are piped to the signal conditioning networks, the audio jack connectors also have a switch used to pipe audio to another device if headphones are unplugged, we took advantage of this to know which and how many sensors are connected. The outputs from the signal conditioning networks were connected directly to the adc using differential pairs, the S channel had to be inverted in polarity as the adc has a unusual 1P,1N,2N,2P,3P,3N pin arrangement, with this inversion both at the audio jack input and the adc inputs the electrical signal polarity is the same as all other channels. A low jitter 8.192Mhz clock generator supply’s the adc’s clock, which can be enabled and disabled via a pin on the raspberry pico.
From the Adc to the raspberry the connection is made using the standard SPI protocol, plus a reset pin (pulled up to 3.3v during normal use) and a Data_Ready Interrupt pin used to trigger a SPI command allowing the µcontroller to efficiently use its clock cycles doing FFT work instead. The Raspberry pico has 2 internal SPI controllers, and both are available at various groups of pins, for the adc the SPI0 controller was used on pins GP2 through GP5.  To finish off the board, the wifi card will use UART0 on pins GP0 and GP1, a radio/SPI port (for external sram) uses the SPI1 controller on pins GP10 through GP13, and the Raspberry is powered by the 3.3 volt regulator through a diode so as not to allow Usb’s 5v to back feed into the rest of the board.


### 5.4 - Simulation
The team used LTspice, to simulate the various circuit conditions, a spice model for the amplifier wasn’t found so the team used a LTC6406 FDA instead, which had better bandwidth but didn’t have rail to rail output, having this differences in mind it still was very helpful to see how the circuit would behave, specially in frequency, and noise performance.

In this first test we show the circuit under max normal sensing load 30A rms, and the differential output.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig6.png)

 ####### fig. 6 - 30A RMS Simulation

This shows how the tvs and setting the rail supply voltages to the right value helps to keep the voltage to the adc inside its -1.5 to 3.5v safe range.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig7.png)

####### fig. 7 - Overvoltage Protection Simulation

Result of a Ac sweep from 10 to 10MHz to show how high frequency noise will be attenuated.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig8.png)

##### fig. 8 - Frequency Response

### 5.5 - PCB board
#### 5.5.1 - Board Overview

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig9.png)

On the board we see three audio jacks serve as the sensor input connectors, one for each phase. The three signals have then to be processed, starting with being filtered with RC circuits to eliminate noise, which are then amplified and sent to the ADC. On the right side there’s the usb connection followed by the 2 pin external power header (for use with batteries), next are the reverse polarity protection mosfet and the main 3.3v regulator. Bottom right lies the rotary encoder for user interface,the I²C interface header to its left and the -1.5v rail to the left of that, the ADC occupies the center position, the µcontroller, wifi card and radio are on the back side. The LCD screen and rotary encoder allows the user to see data in real time, navigate in the menu to select what information to see as well as change the settings such as sample rate, sample time, how many phases to monitor, etc.

#### 5.5.2 - Main Board
The Design was done using Altium Designer, the pcb design industry standard (alongside Allegro) the board shape was made to easily make a 3d printed case in the future if time allows, four 3mm screw points were added for mounting, all three sensor inputs on one side with the power jack on the other side, and a secondary power input to use with an external power supply or battery.
The team chose a 4 layer board to allow the easy routing of the various power planes, and guaranteeing a constant trace impedance by having an uninterrupted ground plane below the 1st layer. A 4 layer board also leads to low impedance to ground and power (planes on layer 3) as vias can be placed right next to the pads minimizing loop area. Care was taken to keep the sensitive areas and signals of the board such as adc and analog amplifiers noise free, by keeping other components far away and placing really noisy parts such as the wifi, radio boards and µcontroller on the other side of the board.
The PCB board was being roughly designed at the same time as the schematic, as to allow the team to spot any problems that could be solved or improved with a schematic change, one happened during the design of the signaling network and connection to the adc where it was noticed that the adc had a unusual pinout, at which point we would either have to pass critical signals such as the S channel through a via or change the polarity of the signals in the schematic to make it correspond with the adc inputs, this schematic change allowed the “twisting” of signals to happen at the audio jack side where the was much more room to route the signals.
With the design completed it was sent to be manufactured by JLCPCB.

#### 5.5.3 - Backup Board

After noticing that our ADC was no longer on the market, we decided to order a new one which was noticeably more difficult to solder. With this problem in the way we decided to design a backup board, with a new ADC, easier to solder, just in case.
We did find one (MCP3564RT-E_ST), but it required some adjustment to the circuit, as with this new ADC we also needed a DC shift, which we didn't have to use with the first one, as well as changing the gain of the amplifier to better suit the new adc, the removal of the -1.5v rail as it was no longer required. Other small changes included adding more capacitors, fixing some wiring.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig10.png)

## 7 - Communication between Arduino and ADC, WiFi

We already tested the Wi-Fi on a breadboard which likely due to the 10cm leads used was not very stable but confirmed that the schematic connections were right, next the team will test it on the final board and we are now getting started with the use of the Arduino IoT Cloud since it is supposed to be simple to use and has features like Data Monitoring, Over The Air Uploads, Dashboard Sharing, etc.

# Major Problems/Difficulties and Corrective actions

We belatedly remembered that we don't have an On and Off button. So whenever you connect a cable to the circuit it starts working immediately. In case we mount our board with the LCD and the button in a ‘box’ powered by a battery, we can put a switch there so that the current reaches the circuit or not in order to resolve this problem generated by our forgetfulness.
Our other problem happened while testing 1 of the 2 boards, due to having to have a voltage limited power supply, a signal source to emulate the sensors, and lastly a oscilloscope to verify the results, there was a short that happened due to how both the oscilloscope the signal generator and the power suply all ground their negative end to earth’s ground, this resulted in the board beginning to misbehave and pull much more current at steady state, further testing seems to indicate that the 3.3v regulator blew, along with 2 amplifiers, because we had another board the team work will continue with no problem and another board will be built at a later time.

# Conclusions

The team has been able to keep up with the schedule (2 day slippage was only due to delivery delays), has the hardware side of the project done, only thing missing is finish testing the hardware, next the team will focus on the software side as well as making a user manual for the MHM board. The team has been working well only having small issues, plan B board was done for caution and ended up not being needed as the original adc was soldered with no issue.

## Biography
(1)-TI Precision Labs

## References
(2)-SCT-013 Split core current transformer
(3)-https://www.diodes.com/assets/Datasheets/AZ1084C.pdf
(4)-https://www.ti.com/lit/ds/symlink/ads131m03.pdf?ts=1650272092402&ref_url=https%253A%252F%252Fwww.mouser.com.tr%252F
(5)- MAX1735 Datasheet - https://pt.mouser.com/datasheet/2/256/MAX1735-1515132.pdf



<link rel="stylesheet" href="/imcaf-gitlab-io/css/width.css">
  <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
  <script>
    mermaid.initialize({ startOnLoad: true });
  </script>

