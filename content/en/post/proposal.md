---
date: 2022-03-15T11:25:05-04:00

featured_image: '/UC_V_FundoClaro-negro.png'
tags: []
title: "Initial Proposal"
disable_share: false
description: ""
---

### Main Supervisor

Prof. Sérgio Manuel  ngelo da Cruz, DEEC-FCTUC

### Authors

Alexandre Miguel Alves Galvão</br>
Bruno Miguel Oliveira Cardoso


## Abstract

These days, there is a strong need to anticipate problems before they happen.
In the industry, engine downtime can be very expensive. What if we could, without turning off the engines, detect signs that something bad is about to happen?
Well, we can.
Through the analysis of the current it is possible to detect anomalies or degrading parts that need to be replaced.
This information is then transmitted in real time so that the user can schedule a repair ahead of time or even shut down the engines, preventing further damage and minimizing the cost of repair.
By using non-invasive sensors to precisely detect the smallest changes, we avoid the security and reliability concerns, maintain safety and simplify the installation of the monitoring system, and by sending this data to the cloud we allow easier monitoring of the system by the customers anywhere anytime.

## Introduction

With the Electrical revolution, the world gained a much more powerful and efficient way to create motion, the AC electrical motor even to this day all throughout the diverse industries one of the constants is the reliance on these powerful electrical motors, be it either for powering heavy machinery, pumping liquids or gasses, Conveyors belts, lifts, trains, etc. But as reliable as they are, they are still susceptible to breaking down due to failures of its bearings, windings, overheating, etc. Such failures can easily lead to significant operational downtime and thus loss of production efficiency and profit, regular maintenance can reduce the risk of failure but it doesn’t eliminate it, luckily most of these failures aren’t caused by instantaneous events, instead they are caused by deterioration overtime of various components. Thus by closely monitoring the motor it's possible to detect the deterioration before it leads to catastrophic failure. Allowing time to schedule the repair minimizing downtime waiting for component repair, delivery and assembly.
Each component has multiple ways in which it deteriorates and in order to detect it there are many signals to monitor such as: motor voltage, current, power, power factor, shaft rpm, acceleration and jerk, vibration, temperature of: the rotor, the bearings, the windings etc. Monitoring all of them would prove expensive and difficult to analyze. Luckily mainly thanks to the magnetic coupling between the rotor and the stator most of these deterioration effects directly or indirectly affect the motor’s current waveform, meaning that a detailed analysis of the current form is enough, reducing the number of required sensors to just 1 per phase.
Current waveform which can be monitored using non-invasive sensors, such as a current transformers and Rogowski Coils, which simply need to be placed around each phase wire (neutral monitoring is useful but not a requirement), minimizing cost of installation and maintaining a high level of safety.

## State of the Art

Carrying out research on similar projects is crucial for us to understand what we actually are going to do in practice to achieve our goal. It was easy to find several projects similar to ours because it’s an area with lots of interest at the moment, most seem focused on current measuring or vibration sensors as a signal source but there are many others, some are using multiple microphones for example, there is also diversity in how the signals are then processed, some are using artificial neural networks, others using known good or bad motors to make a simple range of operation, others like us are using fourier transforms as their main tool. This interest hasn’t seemed to materialize into products in the market as of yet, as the products on market seem more focused on detecting phase faults instead. Next we mention the research projects that standed out the most.

We found this example of a project from the Department of electrical EngineeringUniversity of Baghdad, Iraq,  where, in order to diagnose problems in electric motors and generators without dismantling them, an answer is given with the analysis of the current, because it is possible to detect symptoms through this analysis.
Artificial intelligence was used for condition monitoring of the motors using speed and torque estimation. (1)

In this other project by Prof. Rajkumar Shivankar, Dr.P.K.Sharma and D.B.Bhadane, Fast Fourier Transform (FFT) method is used for analyzing signals to detect gearbox faults. We learn that when a fault is present, the frequency spectrum of the current becomes different from healthy engines. Also this is a cost effective option since a current measurement is easy to implement.
It’s a great example of a project where condition monitoring is done by Motor Current Signature Analysis. (2)

Again, in the international journal of systems applications engineering & development, Motor Current Signature Analysis is used for fault detection because it can easily detect the common engine faults with a high degree of reliability.
A great example of the fundamentals of Motor Current Signature Analysis and condition monitoring of induction motors. (3)

In this last example studied in the Department of Electrical Engineering, São Paulo State University,  the evaluation object is the acoustic noise. This option also stands out for being non-invasive and detects faults in the windings. The acoustic signals were read by four piezoelectric sensors in the engine, and were processed using this new technique which locates the precise position (x,y) of faults by insulation issues. (4)


## Methodology

The objective is to detect anomalies. We can do this by measuring changes in the magnetic field generated by the motor, and we will do this by measuring the current.
We're going to use a non-invasive sensor, some signal conditioning, and develop an algorithm to calculate the measured spectrum.
We will also connect the sensors with a cloud server so that the user with internet connection can view the data and even intervene.
The plan consists of making a basic block overview of the project, then dividing the project into smaller more achievable steps that the team can focus on, which will also help with dividing the work between the members. This same concept will be applied to the software programing, where first the team will make a skeleton of the code/functions that are needed, and then assign those functions to one or multiple members.

### Project objectives
* Detect anomalies and signs of degradation.
* Accurately sense the current of the motor with at least a 8kHz sampling rate.
* Send data in real time to the cloud for further analysis.
* Support 3 phase 240v motors.
### Optimization point
* Minimize the cost of the monitoring device.
* Maximize bandwidth.
* Minimize noise.

## Strategy
Firstly the team will research adcs, signal conditioning and other relevant topics, with the basic research done the focus will be on choosing the main hardware blocks for each stage of the system, due to current market conditions buying hardware quickly is a priority. Next is the main schematic and pcb layout which involves the signal conditioning of the sensor’s waveform, after the simulation of the circuit passes the tests, the layout will be finished and sent for production of the first prototype, which will allow further testing and characterization of the system, at this point any problems should be addressed and corrected so that the finished pcb can be sent to production (if need be). On the software side the team will first figure out which functions are needed, define each function and then assign functions/ part of functions to members of the team.

## Tasks
### 1. Research Signal conditioning and ADCs
Research what is required to achieve a clean well proportioned and biased signal at the adc inputs,
as well as which types adc are best suited for this project, must be precise, have enough channels 
(x3), and a good sample rate.

### 2. Define Project’s specifications, and basic block diagram
Define project’s performance and capability goals, as well as drawing a block diagram with the various stages/blocks 
(adc, µcontroller, signal conditioning, etc), performance goals should be inline with the performance of the components found during 
research, block diagram: easy to read, left to right logic flow. 

### 3. Select the hardware for each block (Sensor, Signal Conditioning, ADC, µController, Wireless connection).
One of the most important steps, care should be taken so that no single component is vastly superior (simply adding cost) 
or inferior to the others bottlenecking the system, so component selection will be reviewed by all team members, with the 
objective of delivering the most performance per unit of cost, while maintaining good performance. Each block has its own 
requirements and important specifications to look for, in the case of the adc its resolution, sampling frequency and input 
range, microcontroller’s is performance, cost, ease of programming, for the signal amplifier it’s noise, voltage offset and bandwidth, etc.

### 4. Design the signal conditioning network.
This step determines the signal that will be read by the adc, minimizing noise, voltage offsets from the proper bias, providing good a antialias filter and protecting the adc from over/under voltage scenarios will be of the utmost importance, to verify the performance of the network a LTspice simulation will be run to confirm its performance during task 5.

### 5. Design the Schematic, PCB board, and run spice simulation
Pen is put to virtual paper to design the whole board, this time having to design all the small but important supporting circuits that will handle voltage regulation, reverse polarity protection, connection between the main blocks, input sockets, etc. There is some risk in building this board using smt devices, but there is a mitigation plan in place, where the team will: use moderately large components avoiding leadless components, use a stainless steel stencil and low temperature solder paste to solder the components avoiding trying to solder small components with a normal soldering iron. 

### 6. Assemble, Test and Calibrate board
Will be an important step where the team’s assumptions will be put to the test, the board’s accuracy, precision, total noise, distortion, dc offset, etc will be measured to be sure everything works as intended, in case it doesn’t the circuit needs to be diagnosed to determine the source of the issue and solve it in the final prototype.

### 7. Communication between Arduino and ADC, WiFi
Initial software development, to support the first tests and calibrations, implement the reading of the adc as well as sending the raw data to a computer for testing and debugging, also implement the sending and receiving of data using the wifi card.

### 8. Signal analysis Programming
Implementation of an algorithm to calculate the spectrum of the measured signal. This will allow us to identify specific spectral components that are indicators of potential faults.
Noise level, signal resolution and sampling frequency will play an important role in allowing for accurate detection of deterioration.

### 9.  Program Cloud Data Stream / Site selection 
Connection of the Arduino platform to a cloud server that we will soon choose so that any user with an Internet connection can keep track of the motor condition and visualize any alarms.

### 10. Final Test, Miscellaneous and unknowns
Final test of the board after any issues found during the first test are resolved, test results will be used in the redaction of the manual/datasheet, if any issues remain or there are features to implement the team will use this allocated time to conclude them.

### 11. Redaction of a Technical manual for the device
Should serve as a datasheet and user manual to allow the user to better understand how to use the device, having within it all technical specifications, supported sensors, cloud information, start up guide, theory of operation, board schematic etc, the data used will be the one gathered during testing of the final board.

## Schedule

{{< mermaid align="center" theme="neutral" >}}
gantt
    title Work Schedule
    dateFormat  DD-MM-YYYY
    section Design
      R&D Front End/ADC  :a1, 05-03-2022, 21d
      Define Project's Specs                 :a2, 12-03-2022, 14d
      Select Hardware                        :a3, 12-03-2022, 14d
      Design Front End                       :a4, 12-03-2022, 14d
      Schematic,PCB,SIM                      :a5, 12-03-2022, 14d
    section Build Prototype
      Assembly, Test and Calibration         :a6, after a5, 28d
    section Software
      Comms to ADC and WIFI                  :a7, 09-04-2022, 21d
      Signal Analysis Programming            :16-04-2022  , 28d
      Program Cloud Data Stream              :a8, 16-04-2022, 21d
    section Final
      Final Test, Misc                       :a9, after a8,   14d
      Technical Manual                       :    after a8,   14d
{{< /mermaid >}}

### Milestones
Next Follows the points in the project where the project changes stages, from design to  first testing, to internet connection, and finally at the end Algorithm implementation and thus a working device, the dates reflect when we expect to be able to deliver on those milestones to the project’s supervisor.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
| March 15th            |April 5th|April 26th| May 10th|
|:---------------------:|:-------:|:--------:|:-------:|
| Design the Schematic,<br>Run spice simulation</br>| Assemble, Test <br>Calibrate board</br>   |    Connection with <br>cloud server</br>     |    Implementation of <br>the detection</br> <br>algorithm</br>   |
{{</table>}}

### Budget
This represents our current budget, it’s only a rough indication of how much the project would cost as the hardware hasn’t been finalized and current market conditions may force the team to change components.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Component              |HyperLink | Cost |
|-----------------------|:-------:|:--------:|
| Non-Invasive AC <br>current sensor</br>        |    [PTRobotics](https://www.ptrobotics.com/sensor-de-corrente/2144-non-invasive-ac-current-sensor-30a.html)    |    3x 8.85€<br>total 26.55€</br>     |
| Raspberry Pi Pico            |  [Mauser](https://mauser.pt/catalog/product_info.php?cPath=1667_2620_1672&products_id=096-9421) |  4.59€  |
| ADC - ADS131M03IRUKR  |    [Mouser](https://pt.mouser.com/ProductDetail/Texas-Instruments/ADS131M03IRUKR?qs=sGAEpiMZZMutXGli8Ay4kPY77QXSdVl213XYKe1nmT4%3D)    |    4.39€     |
|   Voltage Regulator     |    [Mouser](https://pt.mouser.com/ProductDetail/Diodes-Incorporated/AZ1084CD-33TRG1?qs=FKu9oBikfSkIi2fxMEvJQA%3D%3D)    |   0.51€     |
|  Mosfet <br>(Reverse polarity Protection)</br>      |    [Mouser](https://pt.mouser.com/ProductDetail/Vishay-Siliconix/SI4435FDY-T1-GE3?qs=sGAEpiMZZMv0NwlthflBiwi9G3aaa5MxFXjB%2FP7Z3Ho%3D)    |    0.46€    |
|  Wifi Board      |    [PTRobotics](https://www.ptrobotics.com/wifi/3498-wifi-module-esp8266.html)    |    4.31€    |
|    Total    |                |    36.50€    |
{{</table>}}


#### References
research References:

[Analog-to-digital conversion beyond 20 bits – CERN – Nikolai Beev](https://cds.cern.ch/record/2646282/files/ieee_i2mtc_beev_7.pdf)<br />
[TI-PrecisionLabs](https://www.youtube.com/watch?v=DNa1-kzD0M0&list=PLISmVLHAZbTSQE425XcLQOlT-oU4bvG_C)<br />
[Analog Devices ADE7754 ADC-Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7754.pdf)<br />
[Raspberry Pico Datasheet](https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf)<br />
[SCT-013 Split core current transformer](https://pdf1.alldatasheet.com/datasheet-pdf/view/1155087/YHDC/SCT-013/+01W52--XVhEEcUhZGtYw+/datasheet.pdf)<br />
FFT function used for benchmarking the microcontrollers<br />
Practical Filter Design for precision ADCs<br />
[How to achieve Proper grounding by  Rick Hartley of Altium](https://www.youtube.com/watch?v=ySuUZEjARPY)<br />


##### State of the Art references:
(1)  “Various Types of Faults and Their Detection Techniques in Three Phase Induction Motors Fault”
Hayder O. Alwan,Dr. Qais S-Al-Sabbagh
 Department of electrical EngineeringUniversity of Baghdad,Baghdad, Iraq 
https://www.academia.edu/33096235/Various_Types_of_Faults_and_Their_Detection_Techniquesin_Three_Phase_Induction_Motors_Fault

(2) “Fault Diagnosis of Gear Box by Using Motor Current Signature”
D.B.Bhadane, Prof. Rajkumar Shivankar, Dr.P.K.Sharma 
International Journal of Engineering and Technical Research (IJETR)
ISSN: 2321-0869, Volume-2, Issue-11, November 2014 
https://www.academia.edu/9777106/Fault_Diagnosis_of_Gear_Box_by_Using_Motor_Current_Signature

(3) “Motor Current Signature Analysis and its Applications in Induction Motor Fault Diagnosis”
Neelam Mehala, Ratna Dahiya
INTERNATIONAL JOURNAL OF SYSTEMS APPLICATIONS, ENGINEERING & DEVELOPMENT Volume 2, Issue 1, 2007
https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.459.1504&rep=rep1&type=pdf

(4) “Stator Winding Fault Phase Identiﬁcation Using Piezoelectric Sensors in Three-Phase Induction Motors” 
Guilherme Lucas, Marco Rocha, Bruno Castro, José Leão and André Andreoli
Department of Electrical Engineering, São Paulo State University https://www.academia.edu/54843220/Stator_Winding_Fault_Phase_Identification_Using_Piezoelectric_Sensors_in_Three_Phase_Induction_Motors



  <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
  <script>
    mermaid.initialize({ startOnLoad: true });
  </script>


