---
title: "MSM - Monitoramento da Saúde do Motor"

cascade:
  featured_image: '/UC_V_FundoClaro-negro.png'

---

Este foi um projeto feito para o curso de Projeto II da Universidade de Coimbra,
consistia em fazer um sensor inteligente de corrente que detectava problemas em motores elétricos
para que pudessem ser resolvidos antes que levassem a falhas catastróficas e tempo de inatividade da fábrica.

Sabe-se que vivemos em uma sociedade que confia e depende cada vez mais do uso de motores elétricos no dia a dia, por serem confiáveis ​​e baratos. Mas, apesar da boa reputação, eles ainda são suscetíveis a falhas em rolamentos, rotores, estatores, enrolamentos, etc. Graças ao uso em massa desses motores na indústria, há uma forte necessidade de responder a esses problemas em grande escala. Quando acontecem, podem levar a um tempo de inatividade operacional, o que leva a uma menor produção, eficiência e lucro.
Através de um monitoramento cuidadoso e frequente é possível detectar o desgaste de determinados componentes a tempo de evitar tais problemas. Felizmente, estes são sempre revelados na forma de onda da corrente do motor, e assim, através de uma análise detalhada desta mesma corrente, utilizando um sensor de corrente por fase.
Este processo tem baixo custo, baixo tempo de instalação e alto nível de segurança.

Na prática o processo começa com três sensores de corrente que lêem três sinais que são amplificados e enviados simultaneamente para um ADC de três canais para serem digitalizados. Em seguida, esses sinais vão para o processador onde serão aplicadas transformadas rápidas de Fourier (fft's) e algoritmos integrais para obter o espectro de corrente no domínio da frequência e assim informar ao usuário o estado de saúde dos componentes do motor. O resultado é enviado para uma nuvem e fica disponível em um site que nosso processador também está executando como interface de usuário para que as informações possam ser acessadas em qualquer lugar do mundo por meio de um dispositivo com acesso à internet.
Existe uma ampla gama de problemas internos a serem abordados em um motor elétrico, mas o grupo focou no presente trabalho apenas em Excentricidade.
Problemas de excentricidade são revelados em uma faixa específica de frequências que (CONTINUAR)


Reconhecimentos
Este projecto foi desenvolvido por mim, Alexandre Galvão e pelo meu colega, Bruno Cardoso que tem experiência anterior na reparação de motores eléctricos enquanto trabalhava para a Rectibeira Lda em Moçambique, ambos estudantes de Engenharia Electrotécnica e de Computadores. Sob a orientação do Professor Sérgio Cruz, chefe do Laboratório de Máquinas Eléctricas do Departamento de Engenharia Electrotécnica e de Computadores da Universidade de Coimbra, com vasta experiência em projectos que abordam temas semelhantes.


