---
date: 2022-04-09T10:58:08-04:00
description: "Primeiro Relatório para o Projecto IMCAF"
featured_image: '/UC_V_FundoClaro-negro.png'
tags: ["scene"]
title: "Primeiro Relatório"
---

# Introdução

Com a revolução elétrica, o mundo ganhou uma forma muito mais poderosa e eficiente de criar movimento, o motor elétrico AC até hoje em todas as diversas indústrias uma das constantes é a dependência desses potentes motores elétricos, seja para alimentação máquinas pesadas, bombeamento de líquidos ou gases, esteiras transportadoras, elevadores, trens, etc. Mas, por mais confiáveis ​​que sejam, ainda são suscetíveis a quebras devido a falhas de seus rolamentos, enrolamentos, superaquecimento, etc. Tais falhas podem facilmente levar a significativa paralisação operacional e, portanto, perda de eficiência de produção e lucro, a manutenção regular pode reduzir o risco de falha, mas não o elimina, felizmente a maioria dessas falhas não são causadas por eventos instantâneos, mas sim por deterioração de horas extras de vários componentes. Assim, monitorando de perto o motor, é possível detectar a deterioração antes que ela leve a uma falha catastrófica.
 O objetivo do MHM é fazer o sensoriamento e processamento do sinal necessário e posteriormente transmitir para qualquer dispositivo com acesso à internet o estado atual dos motores elétricos em tempo real, para que um usuário que possa, com base nisso, atuar no sentido de evitar danos aconteçam, ou que, caso aconteçam, reduzam a gravidade dos mesmos. Graças à leitura ser feita através de sensores não invasivos, evita preocupações de segurança e confiabilidade, mantém a segurança e simplifica a instalação do sistema de monitoramento. Este relatório descreve as principais tarefas, o trabalho realizado, as alterações em relação ao plano original e inclui também o plano de trabalho para os próximos meses.

# Resumo

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/UC_V_FundoClaro-negro.png)

Rápida teoria de operação, três sinais de corrente lidos pelos transformadores de corrente são amplificados e então enviados simultaneamente para um ADC de três canais para serem digitalizados.
No Microprocessador, os sinais são lidos a partir do ADC e, em seguida, processados ​​usando ffts e integrais, para obter as variáveis ​​necessárias para determinar o estado do motor. O resultado é então enviado para a nuvem, acessível pelo usuário em qualquer lugar com acesso à internet. Para quem se preocupa com a segurança, também suportamos streaming para um computador de rede local ou leituras diretas no LCD, mantendo os dados offline.
Existem três aspectos principais para este projeto, o hardware, o processamento de sinal no µController e a interface do usuário final que pode ser o LCD na placa, um site online ou até mesmo um programa no computador do usuário. De acordo com o cronograma que havíamos planejado e mencionado no relatório anterior, concluímos com sucesso o projeto de hardware (tarefas 1 a 5), ​​estamos atualmente trabalhando na montagem, teste e calibração da placa (tarefa 6) (faltando o LCD), também começamos a testar a conexão wifi em um protótipo de breadboard (tarefa 7) com bons resultados iniciais.
 No momento, estamos dentro do cronograma para entregar o restante das tarefas.
 
 # Status do projeto

A elaboração do projeto envolveu o desenho de um esquemático e placa de circuito impresso, isso levou cerca de 16 horas para ser concluído.
 Nessa ocasião elaboramos o relatório de proposta de projeto e recebemos feedback e orientação de nossos supervisores.
Então tivemos que fazer nossa busca por componentes que satisfizessem nossas especificações de condicionamento de sinal (como resistores, amplificadores, reguladores de tensão, transistores, diodos, etc). Após selecionar o hardware, compramos e esperamos que chegasse, e essa espera atrasou nosso progresso por alguns dias, então o segundo marco (Montar, Testar e Calibrar placa, 5 de abril) foi entregue com 2 dias de atraso, porém o primeiro um estava no prazo (Projete o esquema e execute a simulação de especiarias, 15 de março).
Finalmente conseguimos montar a placa e estamos em fase de testes, ao mesmo tempo em que descobrimos a conexão entre a placa e a internet (wi-fi) usando um protótipo em uma protoboard.
As tarefas de 8 a 11 devem ser iniciadas assim que terminarmos com a conexão arduino para wi-fi.

{{< mermaid align="center" theme="neutral" >}}
gantt
    title Work Schedule
    dateFormat  DD-MM-YYYY
    section Design
      Research Signal Conditioning and ADCs  :a1, 05-03-2022, 7d
      Define Project's specs                 :after a1  , 7d
      Select Hardware for each block         :after a1  , 14d
      Design Signal Conditioning network     :after a1  , 14d
      Design Schematic, PCB, and simulation  :after a1  , 14d
    section Build Prototype
      Assembly, Test and Calibration         :a2, 19-03-2022, 7d
      Test                                   :a3, after a2, 7d
      Calibration                            :after a3  ,  14d
    section Software
      Comms to ADC and WIFI                  :after a3  ,  21d
      Signal Analysis Programming            :09-04-2022  , 28d
      Program Cloud Data Stream              :16-04-2022  , 21d
    section Final
      Final Test, Miscellaneous              :30-04-2022  , 14d
      Redact Technical Manual                :30-04-2022  , 14d
{{< /mermaid >}}

## 1 - Condicionamento de sinal de pesquisa

Nesta fase, precisamos ser capazes de moldar o sinal lido pelos sensores para obter um que possamos usar. Tivemos que atenuar, amplificar, eliminar ruídos e usar um clamping de sinal, que tivemos que aprender que é um circuito eletrônico que suprime as sobretensões acima de sua tensão de ruptura usando diodos TVS (transient voltage), que agem muito rapidamente (alguns tão rápidos quanto como 50ps).
Também tivemos que nos familiarizar com o funcionamento dos amplificadores totalmente diferenciais (eles basicamente têm entradas e saídas diferenciais), bem como o que são filtros anti-aliasing e como implementá-los (é um filtro usado para restringir a largura de banda de um sinal impedindo o adc de “ver” reflexões de ruído de frequência mais alta na frequência de interesse).

## 2 - Diagrama de blocos básico
{{< mermaid align="center" theme="neutral" >}}
graph TD;
    A(Sensor)-->B(Signal conditioning);
    B-->C(ADC);
    C-->|SPI0|D[Raspberry];
    E(Rotary Encoder)-->D;
	D-->|SPI1|F(Radio);
	D-->|Uart|G(WiFi);
	D-->|I2C|H(LCD);
{{< /mermaid >}}

## 3 - Seleção do hardware para cada bloco
### 3.1 - ADC

O conversor analógico para digital definirá as especificações básicas de quão preciso e quanto de taxa de transferência este sistema de monitoramento terá, as especificações mais importantes são resolução efetiva (bits), taxa de transferência em amostras por segundo por canal, desempenho de ruído, número de canais, escala completa Faixa (tensão de entrada máxima recomendada), o µController escolhido pode ser o fator limitante em alta taxa de transferência, de modo que o desempenho do ADC sobre os resultados de benchmarks não será aproveitado.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Specification          | ADE7454 | MCP3564R | MCP3903 | ADS131M03 |
|-----------------------|:-------:|:--------:|:-------:|:---------:|
| Nº of channels        |    6    |    4     |    6    |     3     |
| Resolution            |  24-Bit |  24-Bit  |  24-Bit |   24-Bit  |
| Effective Resolution  |         |  15 Bits | <p style="color :green; ">20 Bits</p>  | 16.7 Bits |
| Master Clock          |         |  20Mhz   |         | 4.096Mhz  |
| Samples/second*       | 26KS/s  | <p style="color :green; ">51.2KS/s</p> | 15KS/s  |   <span style="color:#90ee90">32KS/s</span>  |
| Over Sampling Ratio   |         |    32    |   64    |    <p style="color :green; ">128</p>     |
| FS range              |  ±0.5v  |  <p style="color :green; ">±3.3v</p>   |  ±0.5v  |   <span style="color:#90ee90">±1.2v</span>    |
| Supply Voltage        |   5v    |   3.3v   | A 5v/Dig 3.3v |  3.6v  |
| Cost                  | 11.76€  |  <p style="color :green; ">3.86€</p>    |  4.53€  |   4.22€   |
{{</table>}}
* Amostras por segundo por canal, pois alguns ADCS dividem suas Amostras/s pelo número de canais ativos


O ADS131M03IPWR usando um pacote TSSOP-20 foi escolhido por sua amostragem simultânea, comunicação spi simples de usar, suporte a interrupções e pegada simples de solda.
Mais tarde fomos forçados a trocar a peça ADC pois estava fora de estoque no momento em que estávamos fazendo o pedido, felizmente uma pegada diferente do mesmo ADS131M03 ainda estava disponível o ADS131M03IRUKR que exigia apenas pequenas alterações na placa PCB, esta usada um pacote QFN-20, que era muito menor e, portanto, mais difícil de soldar, para aliviar esse problema, a equipe fez uso de um estêncil de aço inoxidável e pasta de solda.

### 3.2 - Sensor de corrente

Existem 3 tipos principais de sensores: resistores de derivação, corrente hall e transformadores de corrente.
Os resistores de derivação são a opção mais barata, mas não oferecem isolamento elétrico, o que torna o bloco ADC muito mais complexo para um projeto trifásico.
Os sensores de corrente Hall são a segunda opção mais barata com bom desempenho e isolamento, mas requerem uma reflexão cuidadosa devido ao gerenciamento térmico e alta tensão na placa, portanto serão evitados neste projeto.
Transformadores de corrente, eles transformam a corrente medida em uma corrente isolada 100/500/2000/etc vezes menor, é necessário um resistor de carga (interno ou externo) para fornecer uma tensão proporcional à corrente que está sendo medida, existem 3 tipos principais: Split núcleo, núcleo de ar e núcleo sólido. Deve-se tomar cuidado para que o resistor de carga esteja sempre presente ou que o condutor que está sendo medido não tenha corrente circulando por ele, pois sem ele uma alta tensão pode ser emitida pelo transformador de corrente.
Air-core como as bobinas de Rogowski, esses sensores geralmente precisam de um integrador, mas são mais flexíveis na forma como são conectados ao condutor que está sendo medido e têm boa resposta de frequência.
O núcleo sólido geralmente é usado em casos de uso mais permanentes, pois exigem que o condutor seja desconectado e inserido no transformador de corrente.
Os transformadores de corrente de núcleo dividido permitem a abertura do núcleo facilitando a inserção do condutor sem a necessidade de desconectá-lo.
Devido à segurança inerente aos transformadores de corrente, o sensor escolhido será deste tipo, pois os diversos TCs de núcleo dividido permitem maior flexibilidade, assim a equipe optou pelo SCT-013-30A ⁽²⁾.

### 3.3 - Microprocessador

Requerimentos mínimos:

- Suporte Spi (para o ADC)
- Suporte Uart (para o WIFI)
- Capacidade de processamento e memória suficientes para suportar 2k+ Amostras/s, por sensor de corrente
- Suporte para Arduino IDE

 {{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}

|Specification          | Arduino Mega<br> 2560  | Arduino Mega<br> Pro mini 2560 | Arduino Nano | Arduino Nano<br> Every | Raspberry<br> Pico | ESP32 |
|----------------------|:----------:|:---------:|:---------:|:---------:|:-------:|:----------:|
| Architecture          | AVR 8 bit | AVR 8 bit | AVR 8 bit | AVR 8 bit | Arm M0+ | Xtensa LX6 |
| Width                 |   8-Bit   |   8-Bit   |   8-Bit   |   8-Bit   |  32-Bit |  32-Bit    |
| Cores                 |     1     |     1     |     1     |     1     |    2    |      2     |
| Core Frequency        |   16Mhz   |   16Mhz   |   16Mhz   |   20Mhz   | 125Mhz  | 80-240Mhz  |
| RAM                   |    8KB    |    8KB    |    2KB    |    6KB    | 264KB   |   512KB    |
| Serial Interfaces     | 1xSPI,<br>1xI2C<br>4xUART | 1xSPI,<br>1xI2C<br>4xUART | 1xSPI,<br>1xI2C<br>1xUART | 1xSPI,<br>1xI2C<br>4xUART | 2xSPI,<br>2xI2C<br>2xUART | 4xSPI,<br>2xI2C<br>3xUART |
| Supply Voltage        |    5v     |    5v     |    5v     |    5v     |  3.3v   |    3.3v    |
| ADC                   |   10-bit  |   10-bit  |   10-bit  |   10-bit  |  12-bit |  ~12-bit   |
| ADC                   |  4.8KS/s  |  4.8KS/s  |  4.8KS/s  |   ~6KS/s  |  73KS/s | (untested) |
| Cost                  |    35€    |    15€    |    14€    |    10€    |   5€    |    14€     |
{{</table>}}


Um benchmark simples foi feito para se ter uma ideia aproximada do desempenho computacional dos µcontrollers disponíveis, ele compreende uma aproximação do algoritmo fft com 256 amostras, e o resultado é quantas amostras puderam ser calculadas durante um segundo de tempo de processador.
Graças ao seu custo realmente baixo e desempenho líder, o Raspberry Pico foi escolhido.

## 4 - Projete a rede de condicionamento de sinal

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig2.png)


Após a escolha do sensor e do adc, a rede de condicionamento de sinal foi projetada para fazer a ponte entre os dois, e proteger a placa de sinais fora do seu alcance seguro, o sensor escolhido possui um resistor de carga interno de 82Ω, ainda devido ao nosso alcance limitado no ADC (-1,2v a 1,2v), um segundo resistor foi colocado em paralelo de 400Ω para limitar o sinal de entrada a 1,2v pico a pico e ajudar a centralizar o sinal em 0V dc offset, para manter o ruído baixo em um filtro passa-baixo RC foi colocado depois do resistor de carga externo e antes do circuito de amplificação, com um ponto 150kHz -3db, pois o adc tem entradas diferenciais e o sensor também, a equipe optou por ter uma rede totalmente diferencial, usando um amplificador totalmente diferencial o MCP6D11T com Largura de banda de 90 MHZ, capacidade de trilho a trilho e faixa de tensão de pico de 5,5v, que foi fornecida por um Vdd de 3,3v e um Vss de -1,5v garantindo que o ADC esteja protegido o tempo todo, pois está dentro de sua faixa segura, o ganho do FDA foi ajustado para ~2 para atingir 2,4V pe k ao pico quando o sensor está detectando 30A rms, o FDA também possui uma entrada Vocm que ajusta a tensão diferencial do modo comum, neste caso foi configurada para 0v.
Nas entradas do adc foi utilizado um filtro anti-aliasing de acordo com o datasheet do adc. Finalmente, um diodo TVS (UCLAMP2511T.TCT) entre os cabos do sensor prende o sinal, se necessário, para proteger o amplificador e o adc.
Os valores estranhos dos resistores se devem a quais resistores são realmente vendidos em grande quantidade no mercado, alterar um pouco o ganho não afeta o desempenho da placa pois pode ser manuseado durante a calibração e permite a redução do preço de os resistores em quase dez vezes em um caso, para a primeira rodada de placas a equipe usou resistores de 0,5% de precisão em toda a placa e capacitores do tipo C0G para as redes de filtragem por seu dielétrico estável que não altera a capacitância com a tensão aplicada, no No caso de capacitores de bypass, usamos o X5R por seu maior valor de capacitância no mesmo pacote.
Uma descoberta interessante ao fazer este esquema foi que inicialmente R1 e RF tinham o mesmo valor em 1k para manter o ganho necessário para o amplificador, mas depois, ao simular o circuito, percebeu-se que isso significava que em frequências mais altas o ganho do amplificador aumentaria de 2 para um limite de 4 reduzindo ligeiramente a eficácia do filtro RC, então a maior parte do valor da resistência agora é colocada em R1 mantendo o aumento de ganho em altas frequências muito menor.

## 5 - Projete o esquema e execute a simulação de especiarias

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig3.png)

### 5.1 - Circuitos de Energia
Com o resto do circuito projetado, a equipe tinha dados suficientes para projetar o circuito de alimentação, a maior parte do circuito funciona em 3,3v, com os amplificadores também precisando de um trilho de -1,5v, porque usb é 5v ambos, um 3,3v e um - São necessários reguladores de 1,5v, através dos valores máximos de consumo de energia retirados das fichas técnicas dos vários componentes, a equipa descobriu a corrente mínima que cada regulador precisava fornecer.

#### 5.1.1 - A alimentação de 3,3V
A alimentação do nosso circuito é feita através de um conector USB-mini B como fonte de 5V, ou um cabeçalho de pitch de 2,54mm. Então temos que usar reguladores de tensão, um para 3,3V alimentado por USB e outro para -1,5V alimentado por 3,3V. Isso garante estabilidade no circuito e podemos fornecê-lo com a faixa de tensões que o regulador suporta (fig. 4).
O regulador de 3,3V alimenta toda a placa, analisamos a corrente máxima consumida pelos amplificadores, o LCD, o ADC, µcontroller e placa wifi e chegamos a um valor ligeiramente inferior a 1A, a maioria dos reguladores foram classificados para apenas 1A, o que era muito perto do máximo para ser confiável, os próximos eram 1.5A, mas estavam em torno do mesmo preço que o 3A AZ1084CD-3.3TRG1, então o escolhemos para ficar tranquilo.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig4.png)

#### 5.1.2 - The -1.5V supply

Nosso ADC trabalha com entradas diferenciais de -1,2V a 1,2V e de -1,6V seu comportamento não é mais o desejado ⁽⁴⁾ achamos que -1,5V era um bom valor para garantir uma boa margem de erro. os ADCs usam muito pouca energia, então escolhemos o regulador MAX1735EUK50+T, pois ele pode fornecer 200mA(5) de corrente. Para a bomba de carga, ela simplesmente tinha que fornecer corrente suficiente para alimentar o regulador, então escolhemos o SP6661EN-L_TR.

#### 5.1.3 - Proteção contra inversão de polaridade

Aqui usamos um Mosfet de canal P (SI4435FDY-T1-GE3) que atua como um interruptor de alta velocidade que só liga com a polaridade correta, protegendo assim o circuito da polaridade reversa. De acordo com a folha de dados do mosfet, o circuito deve ser protegido até -30V ⁽⁵⁾

### 5.2 - Interface do usuário
A interface do usuário foi feita através de um codificador rotativo, com um botão integrado que nos permite implementar uma interface simples e intuitiva, um filtro Rc em cada botão garante que os saltos rápidos enquanto o botão está se acomodando não afetarão o sinal lido pelo µcontroller , que ocupava 3 entradas digitais do pico de framboesa.
Há também uma porta I2C para permitir o uso de várias telas de lcd.

### 5.3 - Esquema Final

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig5.png)

Partindo do lado analógico, usando conectores de áudio padrão, os sinais dos sensores são canalizados para as redes de condicionamento de sinal, os conectores de áudio também possuem um interruptor usado para canalizar áudio para outro dispositivo se os fones de ouvido estiverem desconectados, aproveitamos isso para saber quais e quantos sensores estão conectados. As saídas das redes de condicionamento de sinal foram conectadas diretamente ao adc usando pares diferenciais, o canal S teve que ser invertido em polaridade pois o adc possui um arranjo incomum de pinos 1P,1N,2N,2P,3P,3N, com esta inversão tanto na entrada de áudio e nas entradas adc a polaridade do sinal elétrico é a mesma de todos os outros canais. Um gerador de clock de 8.192Mhz com jitter baixo fornece o clock do adc, que pode ser habilitado e desabilitado através de um pino no pico de framboesa.
Do Adc para o raspberry a conexão é feita usando o protocolo SPI padrão, mais um pino de reset (puxado até 3,3v durante o uso normal) e um pino de interrupção Data_Ready usado para acionar um comando SPI permitindo que o µcontroller use seus ciclos de clock de forma eficiente fazendo o trabalho FFT em vez disso. O Raspberry pico possui 2 controladores SPI internos, e ambos estão disponíveis em vários grupos de pinos, para o adc o controlador SPI0 foi utilizado nos pinos GP2 a GP5. Para finalizar a placa, a placa wifi usará UART0 nos pinos GP0 e GP1, uma porta rádio/SPI (para sram externo) usa o controlador SPI1 nos pinos GP10 a GP13, e o Raspberry é alimentado pelo regulador de 3,3 volts através de um diodo para não permitir que os 5v do USB voltem a alimentar o resto da placa.


### 5.4 - Simulação
A equipe usou o LTspice, para simular as várias condições do circuito, um modelo de tempero para o amplificador não foi encontrado, então a equipe usou um LTC6406 FDA, que tinha melhor largura de banda, mas não tinha saída de trilho para trilho, tendo essas diferenças em mente ainda foi muito útil ver como o circuito se comportaria, especialmente em frequência e desempenho de ruído.

Neste primeiro teste mostramos o circuito sob carga máxima de detecção normal 30A rms, e a saída diferencial.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig6.png)

Isso mostra como as tvs e definir as tensões de alimentação do trilho para o valor correto ajuda a manter a tensão para o adc dentro de sua faixa segura de -1,5 a 3,5v.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig7.png)

Resultado de uma varredura AC de 10 a 10MHz para mostrar como o ruído de alta frequência será atenuado.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig8.png)

### 5.5 - Placa PCB
#### 5.5.1 - Visão geral 

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig9.png)

Na placa vemos três conectores de áudio servindo como conectores de entrada do sensor, um para cada fase. Os três sinais devem então ser processados, começando por serem filtrados com circuitos RC para eliminar ruídos, que são então amplificados e enviados para o ADC. No lado direito está a conexão usb seguida pelo conector de alimentação externa de 2 pinos (para uso com baterias), em seguida estão o mosfet de proteção de polaridade reversa e o regulador principal de 3,3v. No canto inferior direito está o codificador rotativo para a interface do usuário, o cabeçalho da interface I²C à esquerda e o trilho -1,5v à esquerda, o ADC ocupa a posição central, o µcontroller, a placa wifi e o rádio estão na parte de trás. A tela LCD e o codificador rotativo permitem que o usuário veja os dados em tempo real, navegue no menu para selecionar quais informações ver, bem como alterar as configurações como taxa de amostragem, tempo de amostragem, quantas fases monitorar, etc.

#### 5.5.2 - Placa Principal
O design foi feito usando o Altium Designer, o padrão da indústria de design de PCB (junto com o Allegro), o formato da placa foi feito para criar facilmente uma caixa impressa em 3D no futuro, se o tempo permitir, quatro pontos de parafuso de 3 mm foram adicionados para montagem, todas as três entradas de sensor em um lado com o conector de alimentação do outro lado e uma entrada de alimentação secundária para usar com uma fonte de alimentação externa ou bateria.
A equipe escolheu uma placa de 4 camadas para permitir o roteamento fácil dos vários planos de energia e garantir uma impedância de rastreamento constante por ter um plano de aterramento ininterrupto abaixo da 1ª camada. Uma placa de 4 camadas também leva a uma baixa impedância ao aterramento e à energia (planos na camada 3), pois as vias podem ser colocadas ao lado dos pads, minimizando a área do loop. Teve-se o cuidado de manter as áreas sensíveis e sinais da placa, como amplificadores adc e analógicos livres de ruído, mantendo outros componentes distantes e colocando partes realmente barulhentas como wifi, placas de rádio e µcontroller do outro lado da placa.
A placa PCB estava sendo projetada grosseiramente ao mesmo tempo que o esquemático, de modo a permitir que a equipe detectasse quaisquer problemas que pudessem ser resolvidos ou melhorados com uma alteração esquemática, o que aconteceu durante o projeto da rede de sinalização e conexão com o adc onde notou-se que o adc tinha uma pinagem incomum, ponto em que teríamos que passar sinais críticos como o canal S por uma via ou mudar a polaridade dos sinais no esquema para fazê-lo corresponder com as entradas adc, isso A mudança esquemática permitiu que a “torção” dos sinais acontecesse no lado do conector de áudio, onde havia muito mais espaço para rotear os sinais.
Com o projeto concluído foi enviado para ser fabricado pela JLCPCB.

#### 5.5.3 - Placa de Backup

Depois de perceber que nosso ADC não estava mais no mercado, decidimos encomendar um novo que era visivelmente mais difícil de soldar. Com este problema na forma, decidimos projetar uma placa de backup, com um novo ADC, mais fácil de soldar, por precaução.
Encontramos um (MCP3564RT-E_ST), mas exigiu algum ajuste no circuito, pois com este novo ADC também precisávamos de um deslocamento DC, que não precisamos usar com o primeiro, além de alterar o ganho do amplificador para melhor se adequar ao novo adc, a retirada do trilho -1,5v pois não era mais necessário. Outras pequenas mudanças incluíram a adição de mais capacitores, consertando alguma fiação.

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/fig10.png)


## 7 - Comunicação entre Arduino e ADC, WiFi

Já testamos o Wi-Fi em uma placa de ensaio que provavelmente devido aos cabos de 10cm usados ​​não era muito estável, mas confirmamos que as conexões esquemáticas estavam corretas, em seguida a equipe testará na placa final e agora estamos começando o uso do Arduino IoT Cloud, pois deve ser simples de usar e possui recursos como monitoramento de dados, uploads aéreos, compartilhamento de painel, etc.

# Principais problemas/dificuldades e ações corretivas

Lembramos tardiamente que não temos um botão liga e desliga. Portanto, sempre que você conecta um cabo ao circuito, ele começa a funcionar imediatamente. Caso montemos nossa placa com o LCD e o botão em uma ‘caixa’ alimentada por uma bateria, podemos colocar um interruptor ali para que a corrente chegue ou não ao circuito a fim de resolver esse problema gerado pelo nosso esquecimento.
Nosso outro problema aconteceu durante o teste de 1 das 2 placas, por ter que ter uma fonte de alimentação limitada por tensão, uma fonte de sinal para emular os sensores e por último um osciloscópio para verificar os resultados, houve um curto que aconteceu devido a forma como ambos o osciloscópio o gerador de sinal e a fonte de alimentação todos aterraram sua extremidade negativa ao terra, isso resultou na placa começando a se comportar mal e puxar muito mais corrente em estado estacionário, testes adicionais parecem indicar que o regulador de 3,3v explodiu, junto com 2 amplificadores, pois tínhamos outra placa o trabalho da equipe continuará sem problemas e outra placa será construída posteriormente.

# Conclusões

A equipe conseguiu acompanhar o cronograma (2 dias de atraso foi apenas devido a atrasos na entrega), tem o lado do hardware do projeto feito, a única coisa que falta é terminar de testar o hardware, depois a equipe se concentrará no lado do software além de fazer um manual do usuário para a placa MHM. A equipe vem trabalhando bem só tendo pequenos problemas, a placa do plano B foi feita por precaução e acabou não sendo necessária pois o adc original foi soldado sem nenhum problema.

## Biografia
(1)-Laboratórios de precisão de TI

## Referências
(2)-SCT-013 Transformador de corrente de núcleo dividido
(3)-https://www.diodes.com/assets/Datasheets/AZ1084C.pdf
(4)-https://www.ti.com/lit/ds/symlink/ads131m03.pdf?ts=1650272092402&ref_url=https%253A%252F%252Fwww.mouser.com.tr%252F
(5)- MAX1735 Datasheet - https://pt.mouser.com/datasheet/2/256/MAX1735-1515132.pdf



{{ if .Page.Store.Get "hasMermaid" }}
  <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
  <script>
    mermaid.initialize({ startOnLoad: true });
  </script>
{{ end }}
